﻿using AutoProfile.Domain.Models;
using System.Data.Entity;

namespace AutoProfile.Data
{
    public class AutoProfileContext : DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Refill> Refills { get; set; }
    }
}
