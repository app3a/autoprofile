﻿using AutoProfile.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoProfile.Domain.Models
{
    public class Refill
    {
        public long Id { get; set; }
        public Vehicle Vehicle { get; set; }
        public RefillUnit RefillUnit { get; set; }
        public DistanceUnit DistanceUnit { get; set; }
        public float RefillAmount { get; set; }
        public long Odometer { get; set; }
        public bool IsFullTank { get; set; }
        public DateTime RefillDate { get; set; }
    }
}
