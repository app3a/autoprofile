﻿using AutoProfile.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoProfile.Domain.Models
{
    public class Vehicle
    {
        public Vehicle()
        {
            Refills = new List<Refill>();
        }

        public string Title { get; set; }
        public int BrandId { get; set; }
        public Brand Brand { get; set; }
        public int ModelId { get; set; }
        public Model Model { get; set; }
        public RefillUnit DefaultRefillUnit { get; set; }
        public IList<Refill> Refills { get; set; }
        public DistanceUnit DistanceUnit { get; set; }
    }
}
