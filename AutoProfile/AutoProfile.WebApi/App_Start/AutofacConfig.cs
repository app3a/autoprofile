﻿using Autofac;
using Autofac.Integration.WebApi;
using AutoProfile.Business;
using AutoProfile.Business.Interfaces;
using System.Reflection;
using System.Web.Http;

namespace AutoProfile.WebApi
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            var httpConfig = GlobalConfiguration.Configuration;

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<VehicleManager>().As<IVehicleManager>();

            var container = builder.Build();

            httpConfig.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}