﻿using AutoMapper;
using AutoProfile.Business.Interfaces;
using AutoProfile.Common.ViewModels;
using AutoProfile.Domain.Models;
using System.Web.Http;

namespace AutoProfile.WebApi.Controllers
{
    public class VehicleController : ApiController
    {
        private readonly IVehicleManager _vehicleManager;

        public VehicleController(IVehicleManager vehicleManager)
        {
            _vehicleManager = vehicleManager;
        }

        [HttpPost]
        public VehicleViewModel Create(VehicleViewModel model)
        {
            var vehicle = Mapper.Map<Vehicle>(model);
            var vehicleInfo = _vehicleManager.Create(vehicle);

            return Mapper.Map<VehicleViewModel>(vehicleInfo);
        }
    }
}