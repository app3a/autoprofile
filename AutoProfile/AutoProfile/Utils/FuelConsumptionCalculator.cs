﻿using AutoProfile.Domain.Models;
using AutoProfile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoProfile.Utils
{
    public class FuelConsumptionCalculator
    {
        public IList<FuelEconomyDetail> Calculate(Vehicle vehicle)
        {
            IList<FuelEconomyDetail> calculatedEntries = new List<FuelEconomyDetail>();

            float totalFuel = 0.0f;
            float? odometerStart = null;
            bool fullTankFoundPreviously = false;
            foreach (Refill refill in vehicle.Refills.OrderBy(x => x.RefillDate))
            {
                // We can only start calculating fuel economy after the first fuel full tank fill.
                // That's where the mileage start should be.
                // Then onwards we should accumulate the fuel refilled, until another full tank fill.
                if (odometerStart == null)
                {
                    if (refill.IsFullTank)
                        odometerStart = refill.Odometer;
                }
                else
                    totalFuel += refill.RefillAmount;

                // If we have had two full tank fills, then we can calculate the fuel economy
                // between them.
                if (refill.IsFullTank)
                {
                    if (fullTankFoundPreviously)
                    {
                        FuelEconomyDetail fee = new FuelEconomyDetail()
                        {
                            Date = refill.RefillDate,
                            AmountOfFuel = totalFuel,
                            Distance = refill.Odometer - (float)odometerStart
                        };
                        calculatedEntries.Add(fee);

                        // Reset the values for next calc.
                        totalFuel = 0.0f;
                        odometerStart = refill.Odometer;
                    }
                    fullTankFoundPreviously = true;
                }
            }

            return calculatedEntries;
        }
    }
}