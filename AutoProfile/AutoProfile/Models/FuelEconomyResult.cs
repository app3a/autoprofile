﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoProfile.Models
{
    public class FuelEconomyDetail
    {
        public float Distance { get; set; }
        public float AmountOfFuel { get; set; }
        public DateTime Date { get; set; }
        public decimal Value
        {
            get
            {
                return (decimal) (Distance / AmountOfFuel);
            }
        }
    }
}