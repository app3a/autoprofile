﻿using System;
using System.Runtime;
using Microsoft.Owin;
using Owin;
using Serilog;

[assembly: OwinStartup(typeof(AutoProfile.Startup))]
// DON'T REMOVE : fixing a issue on System.Runtime.dll needing as a facade. Dynamic. on Teamcity build.
//compile with: /reference:System.Runtime.dll
namespace AutoProfile
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            Log.Logger = new LoggerConfiguration()
                .WriteTo.ColoredConsole()
                .WriteTo.Seq("http://localhost:5341")
                .CreateLogger();

            Log.Information("Hello, {Name}!", Environment.UserName);
        }
    }
}
