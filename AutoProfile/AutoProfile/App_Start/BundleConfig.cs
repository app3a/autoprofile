﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace AutoProfile
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/libs").Include(
                    "~/Scripts/libs/jquery.js", 
                    "~/Scripts/libs/angular.js",
                    "~/Scripts/libs/angular-resource.js",
                    "~/Scripts/libs/bootstrap.js",
                    "~/Scripts/libs/less.js",
                    "~/Scripts/libs/chartjs.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                    "~/Scripts/app/directives/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                    "~/Scripts/app/*.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/css/*.css"));

            bundles.Add(new StyleBundle("~/Content/less").Include(
                 "~/Content/less/*.less"));
        }
    }
}
