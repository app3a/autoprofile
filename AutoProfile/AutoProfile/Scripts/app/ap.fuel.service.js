/// <reference path="../typings/angularjs/angular.d.ts"/>

(function () {
	var app = angular.module('ap');

	var fuelService = function ($resource) {
		return {
			getRefills: getRefills
		};

		function getRefills() {
			return [
				{
					"id": "10",
					"refillDate": "2015-06-21",
					"refillAmount": "27",
					"fullTank": "true",
					"pricePerAmount": "1.39",
					"odometer": "23900"
				},
				{
					"id": "11",
					"refillDate": "2015-06-23",
					"refillAmount": "20",
					"fullTank": "false",
					"pricePerAmount": "1.39",
					"odometer": "24150"
				},
				{
					"id": "12",
					"refillDate": "2015-06-28",
					"refillAmount": "30",
					"fullTank": "true",
					"pricePerAmount": "1.39",
					"odometer": "24420"
				},
				{
					"id": "13",
					"refillDate": "2015-07-02",
					"refillAmount": "36",
					"fullTank": "true",
					"pricePerAmount": "1.39",
					"odometer": "24910"
				},
				{
					"id": "14",
					"refillDate": "2015-07-16",
					"refillAmount": "34",
					"fullTank": "true",
					"pricePerAmount": "1.39",
					"odometer": "25340"
				}
			];
		}
	};

	fuelService.$inject = ['$resource'];
	app.service('fuelService', fuelService);
})();
