﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angularjs/angular.d.ts"/>

(function () {
    var app = angular.module('ap');

    var sampleExpenseController = function (service) {
        var vm = this;
        vm.expenses = service.getExpenses();
    };

    sampleExpenseController.$inject = ['ap.sample.expense.service'];
    app.controller('ap.sample.expense.controller', sampleExpenseController);
})();
