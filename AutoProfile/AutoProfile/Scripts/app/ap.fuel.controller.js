/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angularjs/angular.d.ts"/>

(function() {	
	var app = angular.module('ap');
	
	var fuelController = function(service) {
		var vm = this;
		vm.refills = service.getRefills();
	};
	
	fuelController.$inject = ['fuelService'];
	app.controller('ap.fuelController', fuelController);	
})();
