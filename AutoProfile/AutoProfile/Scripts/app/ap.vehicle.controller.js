/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angularjs/angular.d.ts"/>

(function() {	
	var app = angular.module('ap');
	
	var vehicleController = function(service) {
		var vm = this;
		vm.vehicles = service.getVehicles();
	};
	
	vehicleController.$inject = ['ap.vehicleService'];
	app.controller('ap.vehicleController', vehicleController);	
})();
