/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angularjs/angular.d.ts"/>

(function() {	
    var app = angular.module('ap');
	
	var expenseController = function(service) {
		var vm = this;
		vm.expenses = service.getExpenses();
		vm.expenseStatistics = service.getExpenseStatistics();
	};
	
	expenseController.$inject = ['ap.expenseService'];
	app.controller('ap.expenseController', expenseController);	
})();
