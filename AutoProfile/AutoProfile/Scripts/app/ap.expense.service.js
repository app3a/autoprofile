/// <reference path="../typings/angularjs/angular.d.ts"/>

(function () {
	var app = angular.module('ap');

	var expenseService = function ($resource) {
		return {
		    getExpenses: getExpenses,
            getExpenseStatistics: getExpenseStatistics
		};

		function getExpenses() {
			return [
				{
					"id": "100",
					"date": "2015-05-11",
					"amount": "230.0",
					"category": "SRV"
				},
				{
					"id": "101",
					"date": "2015-05-16",
					"amount": "21.2",
					"category": "PAR"
				},
				{
					"id": "102",
					"date": "2015-05-20",
					"amount": "116.5",
					"category": "INS"
				},
				{
					"id": "103",
					"date": "2015-05-28",
					"amount": "100",
					"category": "REP"
				}
			];
		}

		function getExpenseStatistics() {
		    return [
				{
				    value: 300,
				    color: colors.fuel,
				    highlight: "#FF5A5E",
				    label: "Fuel"
				},
                {
                    value: 50,
                    color: colors.parking,
                    highlight: "#5AD3D1",
                    label: "Parking"
                },
                {
                    value: 100,
                    color: colors.repair,
                    highlight: "#FFC870",
                    label: "Repair"
                }
		    ];
		}
	};

	expenseService.$inject = ['$resource'];
	app.service('ap.expenseService', expenseService);
})();
