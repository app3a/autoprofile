﻿/// <reference path="../typings/angularjs/angular.d.ts"/>

(function () {
    var app = angular.module('ap');

    var sampleExpenseService = function ($resource) {
        return {
            getExpenses: getExpenses
        };

        function getExpenses() {
            return [
				{
				    value: 300,
				    color: colors.fuel,
				    highlight: "#FF5A5E",
				    label: "Fuel"
				},
                {
                    value: 50,
                    color: colors.parking,
                    highlight: "#5AD3D1",
                    label: "Parking"
                },
                {
                    value: 100,
                    color: colors.repair,
                    highlight: "#FFC870",
                    label: "Repair"
                }
            ];
        }
    };

    sampleExpenseService.$inject = ['$resource'];
    app.service('ap.sample.expense.service', sampleExpenseService);
})();
