﻿(function () {
    var app = angular.module("ap");
    var pieChartDirective = function () {

        var circleOptions = {
            segmentShowStroke: true,
            segmentStrokeColor: "#fff",
            segmentStrokeWidth: 2,
            percentageInnerCutout: 0, // This is 0 for Pie charts
            animationSteps: 100,
            animationEasing: "easeOutBounce",
            animateRotate: true,
            animateScale: false,
            responsive: true,
            legendTemplate:
                "<ul class=\"<%=name.toLowerCase()%>-legend\">" +
                " <% for (var i=0; i<segments.length; i++){%>" + 
                "  <li>" + 
                "   <span style=\"background-color:<%=segments[i].fillColor%>\"></span>" + 
                "   <%if(segments[i].label){%>" +
                "    <span>" + 
                "     <%=segments[i].label%>" +
                "    </span>" +
                "   <%}%>" + 
                "  </li><%}%>" +
                "</ul>"
        }


        return {
            restrict: "A",
            scope: {
                data: "=",
                putLegendsAt: "@"
            },

            link: function (scope, element, attributes, controller) {
                var ctx = element.get(0).getContext("2d");
                var circleData = scope.data;
                var myNewChart = new Chart(ctx).Pie(circleData, circleOptions);

                // if legends to be put.
                if (scope.putLegendsAt) {
                    var circleLegend = myNewChart.generateLegend();
                    $('#' + scope.putLegendsAt).html(circleLegend);
                }
            }
        }
    };

    app.directive("pieChart", pieChartDirective);
})();