/// <reference path="../typings/angularjs/angular.d.ts"/>

(function () {
	var app = angular.module('ap');

	var vehicleService = function ($resource) {
		return {
			getVehicles: getVehicles
		};

		function getVehicles() {
			return [
				{
					"id": "1",
					"brand": "Toyota",
					"model": "Corolla Conquest",
					"yom": "2010",
					"name": "My Commute"
				},
				{
					"id": "2",
					"brand": "Toyota",
					"model": "Prado",
					"yom": "2015",
					"name": "Family Friendly"
				},
				{
					"id": "3",
					"brand": "Fiat",
					"model": "Mini Cooper",
					"yom": "2012",
					"name": "Rock n Roll"
				},
				{
					"id": "4",
					"brand": "Lexus",
					"model": "One",
					"yom": "2016",
					"name": "Silver"
				}
			];
		}
	};

	vehicleService.$inject = ['$resource'];
	app.service('ap.vehicleService', vehicleService);
})();
