﻿using AutoProfile.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoProfile.Common.ViewModels
{
    public class VehicleViewModel
    {
        public VehicleViewModel()
        {
            
        }

        public string Title { get; set; }
        public int BrandId { get; set; }
        public int ModelId { get; set; }
        public RefillUnit RefillUnit { get; set; }
        public DistanceUnit DistanceUnit { get; set; }
    }
}
