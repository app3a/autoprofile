﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoProfile.Common.Enums
{
    public enum RefillUnit
    {
        Litres,
        Gallons
    }
}
