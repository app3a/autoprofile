﻿using AutoProfile.Business.Interfaces;
using AutoProfile.Data;
using AutoProfile.Domain.Models;

namespace AutoProfile.Business
{
    public class VehicleManager : IVehicleManager
    {
        private readonly AutoProfileContext context;

        public VehicleManager()
        {
        }

        public VehicleManager(AutoProfileContext context)
        {

        }

        public Vehicle Create(Vehicle vehicle)
        {
            var newVehicle = context.Vehicles.Add(vehicle);
            return newVehicle;
        }
    }
}
