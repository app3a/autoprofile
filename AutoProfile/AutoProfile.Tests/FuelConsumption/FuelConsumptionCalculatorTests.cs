﻿using AutoProfile.Common.Enums;
using AutoProfile.Domain.Models;
using AutoProfile.Models;
using AutoProfile.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace AutoProfile.Tests.FuelConsumption
{
    [TestFixture]
    public class FuelConsumptionCalculatorTests
    {
        [Test]
        public void ShouldCorrectlyCalculate_ForTwoConsecutiveFulltankRefills()
        {
            var vehicle = new Vehicle() { DistanceUnit = DistanceUnit.Kilometers, DefaultRefillUnit = RefillUnit.Gallons } ;

            vehicle.Refills.Add(new Refill() { RefillDate = DateTime.Now.AddDays(-1),  IsFullTank = true, Odometer = 2000, RefillAmount = 43 });
            vehicle.Refills.Add(new Refill() { RefillDate = DateTime.Now, IsFullTank = true, Odometer = 2200, RefillAmount = 20 });
            
            var calculator = new FuelConsumptionCalculator();

            IList<FuelEconomyDetail> entries = calculator.Calculate(vehicle);

            Assert.AreEqual(1, entries.Count);
            Assert.AreEqual(200/20, entries[0].Value);
        }

        [Test]
        public void ShouldCorrectlyCalculate_ForMultipleMixedRefills()
        {
            var vehicle = new Vehicle() { DistanceUnit = DistanceUnit.Kilometers, DefaultRefillUnit = RefillUnit.Gallons };

            vehicle.Refills.Add(new Refill()
            {
                RefillDate = DateTime.Now.AddDays(-8),
                IsFullTank = true,
                Odometer = 2000,
                RefillAmount = 43
            });
            vehicle.Refills.Add(new Refill()
            {
                RefillDate = DateTime.Now.AddDays(-4),
                IsFullTank = false,
                Odometer = 2200,
                RefillAmount = 20
            });
            vehicle.Refills.Add(new Refill()
            {
                RefillDate = DateTime.Now.AddDays(-1),
                IsFullTank = true,
                Odometer = 2360,
                RefillAmount = 22
            });


            var calculator = new FuelConsumptionCalculator();

            IList<FuelEconomyDetail> entries = calculator.Calculate(vehicle);

            Assert.AreEqual(1, entries.Count);
            Assert.AreEqual((decimal) (360.0f / 42.0f), entries[0].Value);
        }

        [Test]
        public void ShouldCorrectlyCalculate_ForMultipleMixedMultipleFulltankRefills()
        {
            var vehicle = new Vehicle() { DistanceUnit = DistanceUnit.Kilometers, DefaultRefillUnit = RefillUnit.Gallons };

            vehicle.Refills.Add(new Refill()
            {
                RefillDate = DateTime.Now.AddDays(-8),
                IsFullTank = true,
                Odometer = 2000,
                RefillAmount = 43
            });
            vehicle.Refills.Add(new Refill()
            {
                RefillDate = DateTime.Now.AddDays(-4),
                IsFullTank = false,
                Odometer = 2200,
                RefillAmount = 20
            });
            vehicle.Refills.Add(new Refill()
            {
                RefillDate = DateTime.Now.AddDays(-1),
                IsFullTank = true,
                Odometer = 2360,
                RefillAmount = 22
            });
            vehicle.Refills.Add(new Refill()
            {
                RefillDate = DateTime.Now,
                IsFullTank = true,
                Odometer = 2420,
                RefillAmount = 18
            });

            var calculator = new FuelConsumptionCalculator();

            IList<FuelEconomyDetail> entries = calculator.Calculate(vehicle);

            Assert.AreEqual(2, entries.Count);
            Assert.AreEqual((decimal)(360.0f / 42.0f), entries[0].Value);
            Assert.AreEqual((decimal)(60.0f / 18.0f), entries[1].Value);
        }
    }
}
